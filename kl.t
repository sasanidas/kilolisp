! Kilo LISP, a kilo byte-sized LISP system
! CP/M version, requires T3X/Z
! Nils M Holm, 2019, 2020
! In the public domain

! Differences to the C (DOS, Unix) version:
! - Node pool is smaller (4096 nodes)
! - Atom pool is separate, sacrificing flexibility for performance
! - Maximum load level is 1 instead of 3
! - Error handling is different, because there is no longjmp()
! - Console I/O is direct, see READCON
! - GC is verbose by default (use (GC NIL) to turn silent)

module klisp(t3x);

object	t[t3x];

const	NNODES	= 4096;
const	NAMESZ	= 2048;

const	SYMLEN	= 64;
const	BUFLEN	= 128;
const	PRDEPTH	= 20;
const	NLOAD	= 1;

const	ATOMB	= 0x01;
const	MARKB	= 0x02;
const	SWAPB	= 0x04;

const	NIL	= NNODES+5;
const	EOT	= NNODES+4;
const	DOT	= NNODES+3;
const	RPAREN	= NNODES+2;
const	UNDEF	= NNODES+1;
const	SPCL	= NNODES;

var	Magic;

var	Tag::NNODES;

var	Car[NNODES],
	Cdr[NNODES];

var	Names::NAMESZ, Np;

var	Freelist;

var	Acc, Env;

var	Stack, Mstack;

var	Tmp, Tmpcar, Tmpcdr;

var	Znode;

var	Errflag;

var	Input, Inp, Ink;
var	Inbuf, Buffer::BUFLEN;
var	Rejected;

var	Tmpbuf::NLOAD*BUFLEN;

var	Symbols;
var	Id;

var	Parens;
var	Loads;

var	Verbose_GC;

var	S_apply, S_if, S_ifnot, S_lambda, S_lamstar, S_macro, S_prog,
	S_quote, S_qquote, S_unquote, S_splice, S_setq;
var	S_t, S_cons, S_car, S_cdr, S_atom, S_eq, S_gensym, S_it,
	S_suspend, S_gc, S_eofp, S_load, S_setcar, S_setcdr, S_read,
	S_prin, S_prin1, S_print, S_error, S_symbols;

atomp(n) return n >= SPCL \/ Tag::n & ATOMB;

symbolp(n) return n < SPCL /\ Tag::n & ATOMB;

caar(x) return Car[Car[x]];
cadr(x) return Car[Cdr[x]];
cdar(x) return Cdr[Car[x]];
cddr(x) return Cdr[Cdr[x]];

caadr(x) return Car[Car[Cdr[x]]];
cadar(x) return Car[Cdr[Car[x]]];
caddr(x) return Car[Cdr[Cdr[x]]];
cdadr(x) return Cdr[Car[Cdr[x]]];
cddar(x) return Cdr[Cdr[Car[x]]];

caddar(x) return Car[Cdr[Cdr[Car[x]]]];
cdddar(x) return Cdr[Cdr[Cdr[Car[x]]]];

strlen(s) return t.memscan(s, 0, 32767);
strcmp(a, b) return t.memcomp(a, b, strlen(a)+1);

tty.readc() do var k;
	while (1) do
		k := t.bdos(6, 255);
		if (k) return k;
	end
end

tty.writec(c) t.bdos(6, c);

tty.writes(s) do
	while (s::0) do
		tty.writec(s::0);
		s := s+1;
	end
end

pr(s) tty.writes(s);

nl() do var b::3;
	pr(t.newline(b));
end

var	ntoab::20;

ntoa(n) do var q, p;
	p := @ntoab::19;
	q := p;
	p::0 := 0;
	while (n \/ p = q) do
		p := p-1;
		p::0 := n mod 10 + '0';
		n := n / 10;
	end
	return p;
end

decl print(1);

flushinp() do
	Input := T3X.SYSIN;
	Inbuf := Buffer;
	Inp := 0;
	Ink := 0;
	Rejected := EOT;
end

error(m, n) do
	pr("? ");
	pr(m);
	if (n \= UNDEF) do
		pr(": ");
		print(n);
	end
	nl();
	flushinp();
	Errflag := 1;
	return NIL;
end

fatal(m) do
	error(m, UNDEF);
	pr("? aborting");
	nl();
	halt 1;
end

! Deutsch/Schorr/Waite graph marker

mark(n) do var p, x;
	p := NIL;
	while (1) do
		ie (n >= SPCL \/ Tag::n & MARKB) do
			if (p = NIL) leave;
			ie (Tag::p & SWAPB) do
				x := Cdr[p];
				Cdr[p] := Car[p];
				Car[p] := n;
				Tag::p := Tag::p & ~SWAPB;
				n := x;
			end
			else do
				x := p;
				p := Cdr[x];
				Cdr[x] := n;
				n := x;
			end
		end
		else ie (Tag::n & ATOMB) do
			x := Cdr[n];
			Cdr[n] := p;
			p := n;
			n := x;
			Tag::p := Tag::p | MARKB;
		end
		else do
			x := Car[n];
			Car[n] := p;
			Tag::n := Tag::n | MARKB;
			p := n;
			n := x;
			Tag::p := Tag::p | SWAPB;
		end
	end
end

gc(v) do var i, k;
	if (v \/ Verbose_GC) pr("GC: ");
	mark(Znode);
	mark(Acc);
	mark(Env);
	mark(Symbols);
	mark(Stack);
	mark(Mstack);
	mark(Tmpcar);
	mark(Tmpcdr);
	mark(Tmp);
	k := 0;
	Freelist := NIL;
	for (i=0, NNODES) do
		ie (Tag::i & MARKB) do
			Tag::i := Tag::i & ~MARKB;
		end
		else do
			Cdr[i] := Freelist;
			Freelist := i;
			k := k+1;
		end
	end
	if (v \/ Verbose_GC) do
		pr(ntoa(k));
		pr(" NODES, ");
		pr(ntoa(NAMESZ-Np));
		pr(" CHARS");
		nl();
	end
	return k;
end

cons3(a, d, ta) do var n;
	if (Freelist = NIL) do
		Tmpcdr := d;
		if (\ta) Tmpcar := a;
		gc(0);
		Tmpcar := NIL;
		Tmpcdr := NIL;
		if (NIL = Freelist) do
			error("out of nodes", UNDEF);
			return Znode;
		end
	end
	n := Freelist;
	Freelist := Cdr[Freelist];
	Car[n] := a;
	Cdr[n] := d;
	Tag::n := ta;
	return n;
end

cons(a, d) return cons3(a, d, 0);

nrev(n) do var m, h;
	m := NIL;
	while (n \= NIL) do
		h := Cdr[n];
		Cdr[n] := m;
		m := n;
		n := h;
	end
	return m;
end

save(n) Stack := cons(n, Stack);

unsave(k) do var n;
	while (k) do
		if (NIL = Stack) fatal("stack empty");
		n := Car[Stack];
		Stack := Cdr[Stack];
		k := k-1;
	end
	return n;
end

msave(n) Mstack := cons3(n, Mstack, ATOMB);

munsave() do var n;
	if (NIL = Mstack) fatal("mstack empty");
	n := Car[Mstack];
	Mstack := Cdr[Mstack];
	return n;
end

mksym(s) do var i, n, k;
	k := strlen(s)+1;
	if (k < 2) return NIL;
	if (k + Np >= NAMESZ) do
		error("namepool full", UNDEF);
		return Znode;
	end
	t.memcopy(@Names::Np, s, k);
	n := cons3(Np, NIL, ATOMB);
	Np := Np + k;
	return n;
end

symstr(n) return @Names::Car[n];

findsym(s) do var p;
	p := Symbols;
	while (p \= NIL) do
		if (\strcmp(s, symstr(Car[p])))
			return Car[p];
		p := Cdr[p];
	end
	return NIL;
end

addsym(s, v) do var n;
	n := findsym(s);
	if (n \= NIL) return n;
	n := mksym(s);
	Symbols := cons(n, Symbols);
	ie (v = SPCL)
		Cdr[n] := cons(n, NIL);
	else
		Cdr[n] := cons(v, NIL);
	return n;
end

backspace(c) do
	tty.writes("\b\s\b");
	if (c < '\s') tty.writes("\b\s\b");
end

readcon(b) do var k, n, i, j;
	n := 0;
	while (1) do
		k := tty.readc();
		ie (k = '\r' \/ k = '\n') do
			b::n := k;
			n := n+1;
			b::n := 0;
			nl();
			return n;
		end
		else ie (k = 3) do
			nl();
			error("abort", UNDEF);
			return 0;
		end
		else ie (k = '\b' /\ n) do
			n := n-1;
			backspace(b::n);
		end
		else ie (k = 21 \/ k = 24) do
			while (n) do
				n := n-1;
				backspace(b::n);
			end
		end
		else if ('\s' <= k /\ k <= '~' \/ k = 26) do
			ie (k < '\s') do
				tty.writec('^');
				tty.writec(k+64);
			end
			else
				tty.writec(k);
			b::n := k;
			n := n+1;
			if (n >= BUFLEN) do
				nl();
				return n;
			end
		end
	end
end

rdch() do var c;
	if (Rejected \= EOT) do
		c := Rejected;
		Rejected := EOT;
		return c;
	end
	if (Inp >= Ink) do
		ie (Input = T3X.SYSIN)
			Ink := readcon(Inbuf);
		else
			Ink := t.read(Input, Inbuf, BUFLEN);
		if (Ink < 1) return EOT;
		Inp := 0;
	end
	c := Inbuf::Inp;
	Inp := Inp+1;
	if (c = 26) return EOT;
	return c;
end

rdchci() do var c;
	c := rdch();
	if (c >= SPCL) return c;
	if (c >= 'A' /\ c <= 'Z') return c+32;
	return c;
end

decl	xread(0);

rdlist() do var n, lst, a, count, badpair;
	badpair := "bad pair";
	Parens := Parens+1;
	lst := cons(NIL, NIL);
	save(lst);
	a := NIL;
	count := 0;
	while (1) do
		if (Errflag) return NIL;
		n := xread();
		if (n = EOT) return error("missing ')'", UNDEF);
		if (n = DOT) do
			if (count < 1) return error(badpair, UNDEF);
			n := xread();
			Cdr[a] := n;
			if (n = RPAREN \/ xread() \= RPAREN)
				return error(badpair, UNDEF);
			unsave(1);
			Parens := Parens-1;
			return lst;
		end
		if (n = RPAREN) leave;
		ie (a = NIL) 
			a := lst;
		else
			a := Cdr[a];
		Car[a] := n;
		Cdr[a] := cons(NIL, NIL);
		count := count+1;
	end
	Parens := Parens-1;
	if (a \= NIL) Cdr[a] := NIL;
	unsave(1);
	return count-> lst: NIL;
end

symbolic(c) return c >= 'a' /\ c <= 'z' \/
		   c >= 'A' /\ c <= 'Z' \/
		   c >= '0' /\ c <= '9' \/
		   c = '-'  \/ c = '/';

rdsym(c) do var s::SYMLEN+1, i;
	i := 0;
	while (symbolic(c)) do
		if (c = '/') c := rdchci();
		ie (SYMLEN = i)
			error("long symbol", UNDEF);
		else if (i < SYMLEN) do
			s::i := c;
			i := i+1;
		end
		c := rdchci();
	end
	s::i := 0;
	Rejected := c;
	if (\strcmp(s, "nil")) return NIL;
	return addsym(s, UNDEF);
end

syntax(x) error("syntax", x);

quote(q, n) return cons(q, cons(n, NIL));

rdword() do var s::2, n, c;
	s::1 := 0;
	c := rdchci();
	if (\symbolic(c)) syntax(UNDEF);
	n := cons(NIL, NIL);
	save(n);
	while (1) do
		if (c = '/') c := rdchci();
		s::0 := c;
		Car[n] := addsym(s, UNDEF);
		c := rdchci();
		ie (symbolic(c)) do
			Cdr[n] := cons(NIL, NIL);
			n := Cdr[n];
		end
		else
			leave;
	end
	Rejected := c;
	n := unsave(1);
	return quote(S_quote, n);
end

type(x) error("type", x);

xread() do var c;
	c := rdchci();
	while (1) do
		while (c = ' ' \/ c = '\t' \/ c = '\n' \/ c = '\r') do
			if (Errflag) return NIL;
			c := rdchci();
		end
		if (c \= ';') leave;
		while (c \= '\n' /\ c \= '\r') c := rdchci();
	end
	if (c = EOT \/ c = '%') return EOT;
	if (c = '(') do
		return rdlist();
	end
	if (c = '\'') do
		return quote(S_quote, xread());
	end
	if (c = '@') do
		return quote(S_qquote, xread());
	end
	if (c = ',') do
		c := rdchci();
		if ('@' = c) return quote(S_splice, xread());
		Rejected := c;
		return quote(S_unquote, xread());
	end
	if (c = ')') do
		if (\Parens) error("extra paren", UNDEF);
		return RPAREN;
	end
	if (c = '.') do
		if (\Parens) error("free dot", UNDEF);
		return DOT;
	end
	if (symbolic(c)) do
		return rdsym(c);
	end
	if (c = '#') do
		return rdword();
	end
	syntax(UNDEF);
	return UNDEF;
end

print2(n, d) do
	if (d > PRDEPTH) error("prdepth", UNDEF);
	if (Errflag) return error("stop", UNDEF);
	ie (NIL = n) do
		pr("nil");
	end
	else ie (n = EOT) do
		pr("*eot*");
	end
	else ie (n >= SPCL) do
		pr("*unprintable*");
	end
	else ie (symbolp(n)) do
		ie (Car[n] < 0 \/ Car[n] > NAMESZ)	
			pr("*unprintable*");
		else
			pr(symstr(n));
	end
	else do	! List
		if (\atomp(n) /\ S_lamstar = Car[n]) do
			pr("*closure*");
			return;
		end
		pr("(");
		while (n \= NIL) do
			print2(Car[n], d+1);
			n := Cdr[n];
			ie (symbolp(n)) do
				pr(" . ");
				print2(n, d+1);
				n := NIL;
			end
			else if (n \= NIL) do
				pr(" ");
			end
		end
		pr(")");
	end
end

print(n) print2(n, 0);

lookup(x) do var a ,e;
	e := Env;
	while (e \= NIL) do
		a := Car[e];
		while (a \= NIL) do
			if (caar(a) = x) return cdar(a);
			a := Cdr[a];
		end
		e := Cdr[e];
	end
	return Cdr[x];
end

specialp(n)
	return	n = S_quote \/
		n = S_if \/
		n = S_prog \/
		n = S_ifnot \/
		n = S_lambda \/
		n = S_lamstar \/
		n = S_apply \/
		n = S_setq \/
		n = S_macro;

check(x, k0, kn) do var i, a;
	i := 0;
	a := x;
	while (\atomp(a)) do
		i := i+1;
		a := Cdr[a];
	end;
	if (a \= NIL \/ i < k0 \/ (kn \= %1 /\ i > kn))
		syntax(x);
end

decl	eval(1);

load(s) do var fd, ib, ip, ik, in, re;
	if (Loads >= NLOAD) return error("nested load", UNDEF);
	fd := t.open(s, T3X.OREAD);
	if (fd < 0) return error("load", mksym(s));
	in := Input;
	ip := Inp;
	ik := Ink;
	ib := Inbuf;
	re := Rejected;
	Input := fd;
	Inbuf := @Tmpbuf::(Loads*BUFLEN);
	Inp := 0;
	Ink := 0;
	Rejected := EOT;
	Loads := Loads+1;
	while (\Errflag) do
		Acc := xread();
		if (Acc = EOT) leave;
		eval(Acc);
	end
	t.close(fd);
	Loads := Loads-1;
	Rejected := re;
	Inbuf := ib;
	Ink := ik;
	Inp := ip;
	Input := in;
end

dowrite(fd, b, k)
	if (t.write(fd, b, k) \= k)
		error("image write error", UNDEF);

suspend(s) do var fd, k, buf::20;
	fd := t.open(s, T3X.OWRITE);
	if (fd < 0) error("suspend", mksym(s));
	k := strlen(Magic);
	t.memcopy(buf, Magic, k);
	buf::(k) := (NNODES >> 8);
	buf::(k+1) := NNODES;
	buf::(k+2) := (NAMESZ >> 8);
	buf::(k+3) := NAMESZ;
	buf::(k+4) := (Freelist >> 8);
	buf::(k+5) := Freelist;
	buf::(k+6) := (Symbols >> 8);
	buf::(k+7) := Symbols;
	buf::(k+8) := (Np >> 8);
	buf::(k+9) := Np;
	buf::(k+10) := (Id >> 8);
	buf::(k+11) := Id;
	dowrite(fd, buf, k+12);
	dowrite(fd, Car, NNODES * t.bpw());
	dowrite(fd, Cdr, NNODES * t.bpw());
	dowrite(fd, Tag, NNODES);
	dowrite(fd, Names, NAMESZ);
	t.close(fd);
end

doread(fd, b, k)
	if (t.read(fd, b, k) \= k)
		fatal("image read error");

fasload(s) do var fd, k, n, m, buf::20;
	fd := t.open(s, T3X.OREAD);
	if (fd < 0) return;
	k := strlen(Magic);
	doread(fd, buf, k+12);
	n := buf::k << 8 | buf::(k+1);
	m := buf::(k+2) << 8 | buf::(k+3);
	if (n \= NNODES \/ m \= NAMESZ\/ t.memcomp(buf, Magic, k))
		fatal("bad image");
	Freelist := buf::(k+4) << 8 | buf::(k+5);
	Symbols := buf::(k+6) << 8 | buf::(k+7);
	Np := buf::(k+8) << 8 | buf::(k+9);
	Id := buf::(k+10) << 8 | buf::(k+11);
	doread(fd, Car, NNODES * t.bpw());
	doread(fd, Cdr, NNODES * t.bpw());
	doread(fd, Tag, NNODES);
	doread(fd, Names, NAMESZ);
	t.close(fd);
end

builtin(x) do var s;
	ie (S_car = Car[x]) do
		check(x, 2, 2);
		if (atomp(cadr(x))) type(x);
		return caadr(x);
	end
	else ie (S_cdr = Car[x]) do
		check(x, 2, 2);
		if (atomp(cadr(x))) type(x);
		return cdadr(x);
	end
	else ie (S_eq = Car[x]) do
		check(x, 3, 3);
		return cadr(x) = caddr(x)-> S_t: NIL;
	end
	else ie (S_atom = Car[x]) do
		check(x, 2, 2);
		return atomp(cadr(x))-> S_t: NIL;
	end
	else ie (S_cons = Car[x]) do
		check(x, 3, 3);
		return cons(cadr(x), caddr(x));
	end
	else ie (S_setcar = Car[x]) do
		check(x, 3, 3);
		if (atomp(cadr(x))) type(x);
		Car[cadr(x)] := caddr(x);
		return cadr(x);
	end
	else ie (S_setcdr = Car[x]) do
		check(x, 3, 3);
		if (atomp(cadr(x))) type(x);
		Cdr[cadr(x)] := caddr(x);
		return cadr(x);
	end
	else ie (S_gensym = Car[x]) do
		check(x, 1, 1);
		Id := Id+1;
		s := ntoa(Id);
		s := s-1;
		s::0 := 'G';
		return addsym(s, UNDEF);
	end
	else ie (S_eofp = Car[x]) do
		check(x, 2, 2);
		return EOT = cadr(x)-> S_t: NIL;
	end
	else ie (S_read = Car[x]) do
		check(x, 1, 1);
		return xread();
	end
	else ie (S_prin = Car[x]) do
		check(x, 2, 2);
		print(cadr(x));
		pr(" ");
		return cadr(x);
	end
	else ie (S_prin1 = Car[x]) do
		check(x, 2, 2);
		print(cadr(x));
		return cadr(x);
	end
	else ie (S_print = Car[x]) do
		check(x, 2, 2);
		print(cadr(x));
		nl();
		return cadr(x);
	end
	else ie (S_load = Car[x]) do
		check(x, 2, 2);
		if (\symbolp(cadr(x))) type(x);
		load(symstr(cadr(x)));
		return S_t;
	end
	else ie (S_error = Car[x]) do
		check(x, 2, 3);
		if (\symbolp(cadr(x))) type(x);
		ie (cddr(x) = NIL)
			error(symstr(cadr(x)), UNDEF);
		else
			error(symstr(cadr(x)), caddr(x));
		return UNDEF;
	end
	else ie (S_gc = Car[x]) do
		check(x, 1, 2);
		if (Cdr[x] \= NIL)
			Verbose_GC := cadr(x) \= NIL;
		gc(1);
		return NIL;
	end
	else ie (S_suspend = Car[x]) do
		check(x, 2, 2);
		if (\symbolp(cadr(x))) type(x);
		suspend(symstr(cadr(x)));
		return S_t;
	end
	else ie (S_symbols = Car[x]) do
		check(x, 1, 1);
		return Symbols;
	end
	else do
		syntax(x);
		return UNDEF;
	end
end

cklam(x) do var p;
	check(x, 3, %1);
	p := cadr(x);
	while (\atomp(p)) do
		if (\symbolp(Car[p])) syntax(x);
		p := Cdr[p];
	end
end

struct MSTATES = MHALT, MEXPR, MLIST, MBETA, MRETN, MAPPL, MPRED,
		MNOTP, MSETQ, MPROG;

special(x, pm) do
	if (S_quote = Car[x]) do
		check(x, 2, 2);
		pm[0] := munsave();
		return cadr(x);
	end
	if (S_if = Car[x]) do
		check(x, 4, 4);
		msave(MPRED);
		pm[0] := MEXPR;
		save(cddr(x));
		return cadr(x);
	end
	if (S_prog = Car[x]) do
		pm[0] := MEXPR;
		if (NIL = Cdr[x]) return NIL;
		if (NIL = cddr(x)) return cadr(x);
		msave(MPROG);
		save(cddr(x));
		return cadr(x);
	end
	if (S_ifnot = Car[x]) do
		check(x, 3, 3);
		msave(MNOTP);
		pm[0] := MEXPR;
		save(caddr(x));
		return cadr(x);
	end
	if (S_lambda = Car[x]) do
		cklam(x);
		pm[0] := munsave();
		return cons(S_lamstar, cons(Env, Cdr[x]));
	end
	if (S_lamstar = Car[x]) do
		check(x, 3, -1);
		pm[0] := munsave();
		return x;
	end
	if (S_apply = Car[x]) do
		check(x, 3, 3);
		msave(MAPPL);
		pm[0] := MEXPR;
		save(caddr(x));
		save(NIL);
		return cadr(x);
	end
	if (S_macro = Car[x]) do
		check(x, 2, 2);
		if (atomp(cadr(x)) \/ caadr(x) \= S_lambda)
			syntax(x);
		cklam(cadr(x));
		pm[0] := munsave();
		return cons(S_macro,
			cons(cons(S_lamstar, cons(Env, cdadr(x))),
			     NIL));
	end
	if (S_setq = Car[x]) do
		check(x, 3, 3);
		if (\symbolp(cadr(x))) syntax(x);
		msave(MSETQ);
		pm[0] := MEXPR;
		save(cadr(x));
		return caddr(x);
	end
	syntax(x);
	return UNDEF;
end

bindargs(v, a) do var e, n;
	e := NIL;
	save(e);
	while (\atomp(v)) do
		if (NIL = a) return error("too few args", Acc);
		n := cons(Car[v], cons(Car[a], NIL));
		e := cons(n, e);
		Car[Stack] := e;
		v := Cdr[v];
		a := Cdr[a];
	end
	ie (symbolp(v)) do
		n := cons(v, cons(a, NIL));
		e := cons(n, e);
		Car[Stack] := e;
	end
	else if (a \= NIL) do
		return error("extra args", Acc);
	end
	Env := cons(e, Env);
	unsave(1);
end

funapp(x) do
	Acc := x;
	if (atomp(Car[x]) \/ caar(x) \= S_lamstar)
		syntax(x);
	ie (Mstack \= NIL /\ MRETN = Car[Mstack]) do
		Env := cadar(Acc);
		bindargs(caddar(Acc), Cdr[Acc]);
	end
	else do
		save(Env);
		Env := cadar(Acc);
		bindargs(caddar(Acc), Cdr[Acc]);
		msave(MRETN);
	end
	return cons(S_prog, cdddar(x));
end

expand(x) do var n, m, p;
	if (atomp(x)) return x;
	if (Car[x] = S_quote) return x;
	n := symbolp(Car[x])-> Car[lookup(Car[x])]: UNDEF;
	if (\atomp(n) /\ Car[n] = S_macro) do
		m := cons(Cdr[x], NIL);
		m := cons(S_quote, m);
		m := cons(m, NIL);
		m := cons(cadr(n), m);
		m := cons(S_apply, m);
		save(m);
		n := eval(m);
		Car[Stack] := n;
		n := expand(n);
		unsave(1);
		return n;
	end
	p := x;
	while (\atomp(p)) p := Cdr[p];
	if (symbolp(p)) return x;
	save(x);
	n := NIL;
	save(n);
	p := x;
	while (p \= NIL) do
		m := expand(Car[p]);
		n := cons(m, n);
		Car[Stack] := n;
		p := Cdr[p];
	end
	n := nrev(unsave(1));
	unsave(1);
	return n;
end

eval(x) do var n, m;
	Acc := expand(x);
	msave(MHALT);
	m := MEXPR;
	while (\Errflag) do
		ie (m = MEXPR) do
			if (t.bdos(6,255) = 3) error("stop", UNDEF);
			ie (symbolp(Acc)) do
				n := Car[lookup(Acc)];
				if (n = UNDEF)
					return error("undefined", Acc);
				Acc := n;
				m := munsave();
			end
			else ie (atomp(Acc)) do
				m := munsave();
			end
			else ie (specialp(Car[Acc])) do
				m := MBETA;
			end
			else do
				save(Cdr[Acc]);
				Acc := Car[Acc];
				save(NIL);
				msave(MLIST);
			end
		end
		else ie (m = MLIST) do
			ie (NIL = cadr(Stack)) do
				Acc := nrev(cons(Acc, unsave(1)));
				unsave(1);
				m := MBETA;
			end
			else do
				Car[Stack] := cons(Acc, Car[Stack]);
				Acc := caadr(Stack);
				Car[Cdr[Stack]] := cdadr(Stack);
				msave(m);
				m := MEXPR;
			end
		end
		else ie (m = MAPPL) do
			ie (NIL = Car[Stack]) do
				Car[Stack] := Acc;
				Acc := cadr(Stack);
				msave(MAPPL);
				m := MEXPR;
			end
			else do
				n := unsave(1);
				unsave(1);
				Acc := cons(n, Acc);
				m := MBETA;
			end
		end
		else ie (m = MPRED) do
			n := unsave(1);
			ie (NIL = Acc)
				Acc := cadr(n);
			else
				Acc := Car[n];
			m := MEXPR;
		end
		else ie (m = MNOTP) do
			n := unsave(1);
			ie (NIL = Acc) do
				Acc := n;
				m := MEXPR;
			end
			else do
				m := munsave();
			end
		end
		else ie (m = MBETA) do
			ie (specialp(Car[Acc])) do
				Acc := special(Acc, @m);
			end
			else ie (symbolp(Car[Acc])) do
				Acc := builtin(Acc);
				m := munsave();
			end
			else do
				Acc := funapp(Acc);
				m := MEXPR;
			end
		end
		else ie (m = MRETN) do
			Env := unsave(1);
			m := munsave();
		end
		else ie (m = MSETQ) do
			n := unsave(1);
			Car[lookup(n)] := Acc;
			Acc := n;
			m := munsave();
		end
		else ie (m = MPROG) do
			ie (NIL = cdar(Stack)) do
				Acc := Car[unsave(1)];
				m := MEXPR;
			end
			else do
				Acc := caar(Stack);
				Car[Stack] := cdar(Stack);
				msave(MPROG);
				m := MEXPR;
			end
		end
		else if (m = MHALT) do
			return Acc;
		end
	end
	return NIL;
end

reset() do
	Parens := 0;
	Loads := 0;
	Errflag := 0;
	Acc := NIL;
	Env := NIL;
	Stack := NIL;
	Mstack := NIL;
	Tmpcar := NIL;
	Tmpcdr := NIL;
	Tmp := NIL;
end

init() do
	reset();
	Magic := "KT21";
	Symbols := NIL;
	Freelist := NIL;
	Znode := NIL;
	Np := 0;
	Id := 0;
	flushinp();
	Verbose_GC := %1;
	t.memfill(Tag, 0, NNODES);
	t.memfill(Names, 0, NAMESZ);
	Znode := cons(NIL, NIL);
	S_t := addsym("t", SPCL);
	S_apply := addsym("apply", UNDEF);
	S_if := addsym("if", UNDEF);
	S_ifnot := addsym("ifnot", UNDEF);
	S_lambda := addsym("lambda", UNDEF);
	S_lamstar := addsym("lambda*", UNDEF);
	S_macro := addsym("macro", UNDEF);
	S_prog := addsym("prog", UNDEF);
	S_quote := addsym("quote", UNDEF);
	S_qquote := addsym("qquote", UNDEF);
	S_unquote := addsym("unquote", UNDEF);
	S_splice := addsym("splice", UNDEF);
	S_setq := addsym("setq", UNDEF);
	S_it := addsym("it", UNDEF);
	S_cons := addsym("cons", SPCL);
	S_car := addsym("car", SPCL);
	S_cdr := addsym("cdr", SPCL);
	S_atom := addsym("atom", SPCL);
	S_eq := addsym("eq", SPCL);
	S_eofp := addsym("eofp", SPCL);
	S_setcar := addsym("setcar", SPCL);
	S_setcdr := addsym("setcdr", SPCL);
	S_gensym := addsym("gensym", SPCL);
	S_read := addsym("read", SPCL);
	S_prin := addsym("prin", SPCL);
	S_prin1 := addsym("prin1", SPCL);
	S_print := addsym("print", SPCL);
	S_error := addsym("error", SPCL);
	S_load := addsym("load", SPCL);
	S_gc := addsym("gc", SPCL);
	S_suspend := addsym("suspend", SPCL);
	S_symbols := addsym("symbols", SPCL);
end

do var a::14, c;
	c := t.getarg(1, a, 14);
	init();
	fasload(c>0-> a: "klisp");
	while (1) do
		Car[Znode] := NIL;
		Cdr[Znode] := NIL;
		reset();
		pr("* ");
		Acc := xread();
		if (Errflag) loop;
		if (EOT = Acc) leave;
		Acc := eval(Acc);
		if (Errflag) loop;
		print(Acc);
		Car[Cdr[S_it]] := Acc;
		nl();
	end
	nl();
end
